from flask import Flask
from flask import jsonify

app = Flask(__name__)

@app.route("/webapp")
def webapp():
    return jsonify(message='I am a webapp using Gitlab-Runner.')

@app.route("/")
def main():
    return jsonify(message='Welcome to webapp from Gitlab-Runner. Trigger a deployment Gitlab-Runner!')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
